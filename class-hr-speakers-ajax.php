<?php
/**
 * Created by PhpStorm.
 * User: Brian
 * Date: 9/22/2019
 * Time: 11:43 PM
 */

namespace HeyRed\Speakers;

class HR_Speakers_Ajax {

    public static function hr_speaker_registration() {

        $data = $_POST['vars'];

        // Create post object
        $my_post = array(
            'post_type'     => 'hr_speaker',
            'post_title'    => $data['full_name'],
            'post_status'   => 'draft',
        );

        // Insert the post into the database
        $new_post_id = wp_insert_post( $my_post );

        update_post_meta( $new_post_id, 'email', $data['email'] );
        update_post_meta( $new_post_id, 'phone', $data['phone'] );
        update_post_meta( $new_post_id, 'biography', $data['biography'] );
        update_post_meta( $new_post_id, 'add_street', $data['add_street'] );
        update_post_meta( $new_post_id, 'add_street_2', $data['add_street_2'] );
        update_post_meta( $new_post_id, 'add_city', $data['add_city'] );
        update_post_meta( $new_post_id, 'add_state', $data['add_state'] );
        update_post_meta( $new_post_id, 'add_zip', $data['add_zip'] );
        update_post_meta( $new_post_id, 'hours_spoken', $data['hours_spoken'] );
        update_post_meta( $new_post_id, 'presentations', $data['presentations'] );
        update_post_meta( $new_post_id, 'largest_crowd', $data['largest_crowd'] );
        update_post_meta( $new_post_id, 'program_1', $data['program_1'] );
        update_post_meta( $new_post_id, 'program_1_description', $data['program_1_description'] );
        update_post_meta( $new_post_id, 'program_2', $data['program_2'] );
        update_post_meta( $new_post_id, 'program_2_description', $data['program_2_description'] );
        update_post_meta( $new_post_id, 'testimonial_1', $data['testimonial_1'] );
        update_post_meta( $new_post_id, 'testimonial_1_author', $data['testimonial_1_author'] );
        update_post_meta( $new_post_id, 'testimonial_2', $data['testimonial_2'] );
        update_post_meta( $new_post_id, 'testimonial_2_author', $data['testimonial_2_author'] );
        update_post_meta( $new_post_id, 'testimonial_3', $data['testimonial_3'] );
        update_post_meta( $new_post_id, 'testimonial_3_author', $data['testimonial_3_author'] );

        // Return our form message
        wp_send_json( get_option('hr_speaker_registration_form_confirmation', 'Thanks!') );
    }

}