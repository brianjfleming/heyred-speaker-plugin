<?php
/**
 * Adds shortcode and widget for outputting the registration form
 *
 * @package brianjfleming/heyredspeakers
 */

/**
 * Class HR_Speakers_Registration_Form Widget
 */
class HR_Speakers_Registration_Form extends \WP_Widget
{

    function __construct()
    {
        parent::__construct(
            'hr_speakers_registration_form',
            'HeyRed Speaker Registration Form',
            array('description' => 'Outputs a pre-build registration form for the hr_speaker post type')
        );
    }

    /**
     * @param $args
     * @param $instance
     */
    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];

        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        echo \HeyRed\Speakers\HR_Speakers_Shortcodes::hr_speaker_registration_form();

        echo $args['after_widget'];
    }

    /**
     * @param $instance
     */
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = '';
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo 'Title:'; ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
    }

    /**
     * @param $new_instance
     * @param $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}