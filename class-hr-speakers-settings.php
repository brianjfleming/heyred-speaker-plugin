<?php
/**
 * Builds our Settings Page
 *
 * @package brianjfleming/heyredspeakers
 */

namespace HeyRed\Speakers;

/**
 * Class HR_Speakers_Settings
 */
class HR_Speakers_Settings {

    /**
     * Base Setup
     */
    public static function setup() {
        add_action( 'admin_menu', array( '\\' . __NAMESPACE__ . '\\HR_Speakers_Settings', 'register_menu' ) );
    }

    /**
     * Creates our Settings Page
     */
    public static function register_menu() {
        add_submenu_page(
            'edit.php?post_type=hr_speaker',
            'Documentation',
            'Documentation',
            'manage_options',
            'documentation',
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers_Settings', 'output' )
        );
    }

    /**
     * Output for the Settings Page
     */
    public static function output() {

        if ( isset( $_POST['hr_speaker_registration_form_confirmation'] ) ) {
            update_option( 'hr_speaker_registration_form_confirmation', $_POST['hr_speaker_registration_form_confirmation'] );
        }


        ?>
        <div class="wrap">

            <h1>HeyRed Speaker Settings and Documentation</h1>

            <div class="postbox" style="margin-top: 2em;">
                <div class="inside">

                    <h3>Registration Form Confirmation Message</h3>

                    <form method="post">

                        <div class="form-element form-textarea">
                            <textarea id="hr_speaker_registration_form_confirmation" name="hr_speaker_registration_form_confirmation" class="form-element-field" placeholder="Please type a message here you would like to display to users after they fill out the Speaker Registration Form..."  data-maxlength="100" data-unit="word"><?php echo get_option( 'hr_speaker_registration_form_confirmation', '' ) ?></textarea>
                            <div class="form-element-bar"></div>
                            <small class="form-element-hint"></small>
                        </div>

                        <?php submit_button(); ?>

                    </form>

                </div>
            </div>

            <div class="postbox" style="margin-top: 2em;">
                <div class="inside">

                    <h3>Outputting the Speaker Registration Form</h3>

                    <p>
                        This plugin includes a prebuild speaker registration form, which will allow access to automatically register a new 'hr_speaker' post type.
                        This form will ONLY create a new post as a draft, and approval must be granted by the site admin to publish any newly registered speaker.
                        To output the form, you may either add the 'HeyRed Speaker Registration Form' widget to a page via customizer, or utilize the <code>[hr_speaker_registration_form]</code> shortcode.
                    </p>

                    <p>
                        The editable confirmation text above will allow you to customize the message a user sees after filling out this registration form.
                    </p>

                    <h3>Outputting Speaker Post Meta</h3>

                    <p>
                        Shortcodes have been made available for use for each item of postmeta for the hr_speaker custom post type.
                        The following shortcodes are available for use, but will only return data when used on a single view for an 'hr_speaker' post type.
                    </p>

                    <p>
                        These can be used in widgets, or can be used in a single post template by placing the shortcode inside of a do_shortcode function: <code>&lt;?php do_shortcode( '[hr_speaker_name]' ?&gt;</code>
                    </p>

                    <ul>
                        <li>[hr_speaker_name]</li>
                        <li>[hr_speaker_email]</li>
                        <li>[hr_speaker_phone]</li>
                        <li>[hr_speaker_biography]</li>
                        <li>[hr_speaker_add_street]</li>
                        <li>[hr_speaker_add_street_2]</li>
                        <li>[hr_speaker_add_city]</li>
                        <li>[hr_speaker_add_state]</li>
                        <li>[hr_speaker_add_zip]</li>
                        <li>[hr_speaker_hours_spoken]</li>
                        <li>[hr_speaker_presentations]</li>
                        <li>[hr_speaker_largest_crowd]</li>
                        <li>[hr_speaker_program_1]</li>
                        <li>[hr_speaker_program_1_description]</li>
                        <li>[hr_speaker_program_2]</li>
                        <li>[hr_speaker_program_2_description]</li>
                        <li>[hr_speaker_testimonial_1]</li>
                        <li>[hr_speaker_testimonial_1_author]</li>
                        <li>[hr_speaker_testimonial_2]</li>
                        <li>[hr_speaker_testimonial_2_author]</li>
                        <li>[hr_speaker_testimonial_3]</li>
                        <li>[hr_speaker_testimonial_3_author]</li>
                    </ul>

                </div>
            </div>
        </div>


        <?php

    }

}