var $ = jQuery.noConflict();

// Setup validation rules
$( document ).on( 'ready', function() {

    $("form#hr-speaker-registration-form").validate({
        // Specify validation rules
        rules: {
            full_name: "required",
            email: {
                required: true,
                email: true
            },
            phone: "required",
            biography: {
                required: true,
                maxlength: 200,
                minlength: 1
            },
            add_street: "required",
            add_city: "required",
            add_state: "required",
            add_zip: {
                required: true,
                maxlength: 5
            },
            hours_spoken: {
                required: true,
                digits: true
            },
            presentations: {
                required: true,
                digits: true
            },
            largest_crowd: {
                required: true,
                digits: true
            },
            program_1: "required",
            program_1_description: "required",
            program_2: "required",
            program_2_description: "required"
        },
        // Specify validation error messages
        messages: {
            full_name: "Please enter your full name",
            email: "Please enter a valid email address",
            phone: "Please Enter a valid phone number",
            biography: {
                required: "Please enter a brief biography",
                maxlength: "Please use a maximum of 200 characters."
            },
            add_street: "Please enter a street address",
            add_city: "Please enter a city",
            add_state: "Please enter a state",
            add_zip: "Please enter a valid zip code",
            hours_spoken: "Please provide hours spoken, as a number",
            presentations: "Please provide total presentations, as a number",
            largest_crowd: "Please provide largest crowd, as a number",
            program_1: "Please enter a program name",
            program_1_description: "Please enter a program description",
            program_2: "Please enter a program name",
            program_2_description: "Please enter a program description"
        },
        submitHandler: function(form) {
            submit_form( form );
        }
    });

});

function submit_form( form ) {
    var data = convert_form_to_json( form );

    $.ajax({
        type: "POST",
        url: loc.ajaxurl,
        data: {
            action: 'hr_speaker_registration',
            vars: data
        },
        dataType: "json",
        success: function(response) {
            set_form_confirmation(response);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            set_form_confirmation(xhr.status + ':' + thrownError + '<br>' + xhr.responseText);
        }
    });
}

function convert_form_to_json(form){
    var array = jQuery(form).serializeArray();
    var json = {};

    jQuery.each(array, function() {
        json[this.name] = this.value || '';
    });

    return json;
}

function set_form_confirmation( message ) {
    $('form#hr-speaker-registration-form').replaceWith( '<div class="registration-response">' + message + '</div>' );
}