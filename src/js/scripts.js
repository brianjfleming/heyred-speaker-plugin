(function($) {

    // Document Ready Trigger
    $( document ).on( 'ready', function() {

        init_textcounter();

    });

    // Apply 'textcounter' to any .form-element-field with a maxlength attribute
    function init_textcounter() {

        // All matching elements and loop
        var elements = $('.form-element-field[data-maxlength]');

        $.each( elements, function() {

            // set initial counter state
            update_textcounter( $(this) );

            // set event listener on keyup
            $(this).on( 'keyup', function() {
                update_textcounter( $(this) );
            } );

        });

    }

    function update_textcounter( ele ) {

        var length = ele.val().length;
        var unit_label = "characters";
        var unit = ele.attr('data-unit'); // "word" or "char" - default "char"

        // For "word"
        if( unit === "word" ) {
            var length_check = $.trim(ele.val()).split(" ");
            length = length_check.length;
            unit_label = "words";
        }

        // Get maxlength and current length
        var maxlength = ele.attr('data-maxlength');

        // Get Hint (if exists)
        var hint = ele.siblings('.form-element-hint');
        hint.html( length + " of " + maxlength + " " + unit_label + " used" );

        // Compare and set states
        if ( length > maxlength ) {

            ele.parents('.form-element').addClass('form-has-error');

            if( unit === "word" ) {
                var new_value = length_check.slice(0, (maxlength)).join(' ');
                ele.val( new_value );
            } else {
                ele.val( ele.val().substring( 0, maxlength ) );
            }
            return false;
        } else {
            ele.parents('.form-element').removeClass('form-has-error');
        }

    }

})(jQuery);

