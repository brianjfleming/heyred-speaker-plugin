<?php
/**
 * HeyRed Speakers Plugin
 *
 * @package     brianjfleming/heyredspeakers
 * @author      Brian Fleming
 * @copyright   2019 Brian Fleming
 * @license     GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Hey Red Speakers
 * Plugin URI:  https://bitbucket.org/brianjfleming/heyred-speaker-plugin/src/master/
 * Description: HeyRed Speakers Plugin.
 * (1) Creates a new WordPress Custom Post Type called 'hr_speaker'
 * (2) Creates a custom GravityForm for creating a new post automatically
 * (3) Registers a set of utility shortcodes used for easy access to custom post meta
 * Version:     1.4.1
 * Author:      Brian Fleming
 * License:     GPL v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

namespace HeyRed\Speakers;

define( 'HEYRED_SPEAKERS_VERSION', '1.4.1' );

// Custom Post Type Registration
require_once 'class-hr-speaker.php';
add_action( 'init', array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'register_hr_speakers' ) );

// Settings Page
require_once 'class-hr-speakers-settings.php';
add_action( 'plugins_loaded', array( '\\' . __NAMESPACE__ . '\\HR_Speakers_Settings', 'setup' ) );

require_once 'class-hr-speakers-shortcodes.php';
add_action( 'init', array( '\\' . __NAMESPACE__ . '\\HR_Speakers_Shortcodes', 'setup' ) );

require_once 'class-hr-speakers-registration-form.php';
add_action( 'widgets_init', function() { register_widget( 'HR_Speakers_Registration_Form' ); } );

require_once 'class-hr-speakers-ajax.php';
add_action( 'wp_ajax_hr_speaker_registration', array( '\\' . __NAMESPACE__ . '\\HR_Speakers_Ajax', 'hr_speaker_registration' ) );
add_action( 'wp_ajax_nopriv_hr_speaker_registration', array( '\\' . __NAMESPACE__ . '\\HR_Speakers_Ajax', 'hr_speaker_registration' ) );

// Register Scripts and Styles
add_action( 'init', function() {

    wp_register_style( 'heyred-admin-styles', plugins_url( 'dist/css/admin_styles.min.css', __FILE__ ) );
    wp_register_style( 'heyred-form-styles', plugins_url( 'dist/css/material_forms.min.css', __FILE__ ) );
    wp_register_script( 'heyred-scripts', plugins_url( 'dist/js/scripts.min.js', __FILE__ ), array( 'jquery' ), HEYRED_SPEAKERS_VERSION );

});

// Enqueue Admin Styles
add_action( 'admin_enqueue_scripts', function() {
    wp_enqueue_style( 'heyred-admin-styles' );

    wp_enqueue_style( 'heyred-form-styles' );

    wp_enqueue_script( 'heyred-scripts' );
    wp_localize_script( 'heyred-scripts', 'loc', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
});

add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'heyred-form-styles' );

    wp_enqueue_script( 'wp-utils' );

    wp_enqueue_script( 'heyred-scripts' );
    wp_localize_script( 'heyred-scripts', 'loc', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
});