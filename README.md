# Hey Red Speakers Plugin

* Creates a new WordPress Custom Post Type called 'hr_speakers'
* Creates a custom GravityForm for creating a new post automatically
* Registers a set of utility shortcodes used for easy access to custom post meta

# Dev Notes

Ensure webpack and webpack-cli are installed globally
```$xslt
$ npm install -g webpack
$ npm install -g webpack-cli
```

Run `npm install` to install dependencies.

Use `npm start` to watch SRC files for changes

Use `npm run build` to create minified files for distribution

# Change Log

## 1.4.1
* Update Plugin Website (points to bitbucket)

## 1.4.0
* Adds 'editor' support to the hr-speaker custom post type (DIVI Support)

## 1.3.0
* Update styles for form buttons

## 1.2.0
* Form validation rules
* Finalize Error handling and testing

## 1.1.2
* Bug fix for registration form and sometimes causin ga 500 error.

## 1.1
* Base Plugin finalization, Releace Candidate for deployment to HeyRed.biz