<?php
/**
 * Registers the 'hr_speaker' custom post type
 *
 * @package brianjfleming/heyredspeakers
 */

namespace HeyRed\Speakers;

/**
 * Class HR_Speakers
 */
class HR_Speakers {

    /**
     * Register the Custom Post Type
     */
    public static function register_hr_speakers() {

        // Post Type Args.
        $args = array(
            'labels'             => self::labels(),
            'description'        => 'Speakers.',
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'speakers' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'menu_icon'          => 'dashicons-businessperson',
            'hierarchical'       => false,
            'menu_position'      => 5,
            'supports'           => array( 'title', 'editor', 'thumbnail' )
        );

        // Register Post Type.
        register_post_type( 'hr_speaker', $args );

        // Register Custom Post Meta
        add_action( 'add_meta_boxes', array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'register_postmeta_boxes') );

        // Save Custom Post Meta
        add_action( 'save_post_hr_speaker', array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'save_postmeta') );
    }

    /**
     * Register custom postmeta
     */
    public static function register_postmeta_boxes() {

        // Contact Information.
        add_meta_box(
            'hr_speaker_contact',           
            'Contact Information',  
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_contact' ),  
            'hr_speaker'                   
        );

        // Contact Information.
        add_meta_box(
            'hr_speaker_biography',
            'Speaker Biography',
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_biography' ),
            'hr_speaker'
        );

        // Address
        add_meta_box(
            'hr_speaker_address',           
            'Address',  
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_address' ),  
            'hr_speaker'                   
        );

        // History
        add_meta_box(
            'hr_speaker_history',           
            'History',  
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_history' ),  
            'hr_speaker'                   
        );

        // Programs
        add_meta_box(
            'hr_speaker_program_1',
            'Program #1',
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_program_1' ),
            'hr_speaker'
        );

        // Programs
        add_meta_box(
            'hr_speaker_program_2',
            'Program #2',
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_program_2' ),
            'hr_speaker'
        );

        // Testimonials
        add_meta_box(
            'hr_speaker_testimonial_1',
            'Testimonial #1',
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_testimonial_1' ),
            'hr_speaker'                   
        );

        // Testimonials
        add_meta_box(
            'hr_speaker_testimonial_2',
            'Testimonial #2',
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_testimonial_2' ),
            'hr_speaker'
        );

        // Testimonials
        add_meta_box(
            'hr_speaker_testimonial_3',
            'Testimonial #3',
            array( '\\' . __NAMESPACE__ . '\\HR_Speakers', 'render_hr_speaker_testimonial_3' ),
            'hr_speaker'
        );

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_contact( $post ) {

        ?>

        <div class="form-element form-input">
            <input id="email" name="email" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'email', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="email">Email Address</label>
        </div>

        <div class="form-element form-input">
            <input id="phone" name="phone" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'phone', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="phone">Phone Number</label>
        </div>

        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_biography( $post ) {

        ?>

        <div class="form-element form-textarea">
            <textarea id="biography" name="biography" class="form-element-field" placeholder=""  data-maxlength="200"><?php echo get_post_meta( $post->ID, 'biography', true ) ?></textarea>
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="biography">Biography</label>
            <small class="form-element-hint"></small>
        </div>

        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_address( $post ) {

        ?>

        <div class="form-element form-input">
            <input id="add_street" name="add_street" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'add_street', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="add_street">Street Address</label>
        </div>

        <div class="form-element form-input">
            <input id="add_street_2" name="add_street_2" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'add_street_2', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="add_street_2">Line 2</label>
        </div>

        <div class="form-element form-input">
            <input id="add_city" name="add_city" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'add_city', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="add_city">City</label>
        </div>

        <div class="form-element form-input">
            <input id="add_state" name="add_state" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'add_state', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="add_state">State</label>
        </div>

        <div class="form-element form-input">
            <input id="add_zip" name="add_zip" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'add_zip', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="add_zip">Zip Code</label>
        </div>

        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_history( $post ) {

        ?>

        <div class="form-element form-input">
            <input id="hours_spoken" name="hours_spoken" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'hours_spoken', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="hours_spoken">Hours Spoken</label>
        </div>

        <div class="form-element form-input">
            <input id="presentations" name="presentations" class="form-element-field" placeholder="How many presentations has this speaker given?" value="<?php echo get_post_meta( $post->ID, 'presentations', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="presentations">Presentations Given</label>
        </div>

        <div class="form-element form-input">
            <input id="largest_crowd" name="largest_crowd" class="form-element-field" placeholder="What is the largest crowd this speaker has presented to?" value="<?php echo get_post_meta( $post->ID, 'largest_crowd', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="largest_crowd">Largest Crowd Size</label>
        </div>


        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_program_1( $post ) {

        ?>

        <div class="form-element form-input">
            <input id="program_1" name="program_1" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'program_1', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="program_1">Program Name</label>
        </div>

        <div class="form-element form-textarea">
            <textarea id="program_1_description" name="program_1_description" class="form-element-field" placeholder=""  data-maxlength="20" data-unit="word"><?php echo get_post_meta( $post->ID, 'program_1_description', true ) ?></textarea>
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="program_1_description">Program Description</label>
            <small class="form-element-hint"></small>
        </div>

        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_program_2( $post ) {

        ?>

        <div class="form-element form-input">
            <input id="program_2" name="program_2" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'program_2', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="program_2">Program Name</label>
        </div>

        <div class="form-element form-textarea">
            <textarea id="program_2_description" name="program_2_description" class="form-element-field" placeholder=""  data-maxlength="20" data-unit="word"><?php echo get_post_meta( $post->ID, 'program_2_description', true ) ?></textarea>
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="program_2_description">Program Description</label>
            <small class="form-element-hint"></small>
        </div>

        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_testimonial_1( $post ) {

        ?>

        <div class="form-element form-textarea">
            <textarea id="testimonial_1" name="testimonial_1" class="form-element-field" placeholder=""  data-maxlength="20" data-unit="word"><?php echo get_post_meta( $post->ID, 'testimonial_1', true ) ?></textarea>
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="testimonial_1">Testimonial</label>
            <small class="form-element-hint"></small>
        </div>

        <div class="form-element form-input">
            <input id="testimonial_1_author" name="testimonial_1_author" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'testimonial_1_author', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="testimonial_1_author">Testimonial Author</label>
        </div>

        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_testimonial_2( $post ) {

        ?>

        <div class="form-element form-textarea">
            <textarea id="testimonial_2" name="testimonial_2" class="form-element-field" placeholder=""  data-maxlength="20" data-unit="word"><?php echo get_post_meta( $post->ID, 'testimonial_2', true ) ?></textarea>
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="testimonial_2">Testimonial</label>
            <small class="form-element-hint"></small>
        </div>

        <div class="form-element form-input">
            <input id="testimonial_2_author" name="testimonial_2_author" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'testimonial_2_author', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="testimonial_2_author">Testimonial Author</label>
        </div>

        <?php

    }

    /**
     * Render Metabox
     *
     * @param object $post - The post Object.
     */
    public static function render_hr_speaker_testimonial_3( $post ) {

        ?>

        <div class="form-element form-textarea">
            <textarea id="testimonial_3" name="testimonial_3" class="form-element-field" placeholder=""  data-maxlength="20" data-unit="word"><?php echo get_post_meta( $post->ID, 'testimonial_3', true ) ?></textarea>
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="testimonial_3">Testimonial</label>
            <small class="form-element-hint"></small>
        </div>

        <div class="form-element form-input">
            <input id="testimonial_3_author" name="testimonial_3_author" class="form-element-field" placeholder=" " value="<?php echo get_post_meta( $post->ID, 'testimonial_3_author', true ) ?>" />
            <div class="form-element-bar"></div>
            <label class="form-element-label" for="testimonial_3_author">Testimonial Author</label>
        </div>

        <?php

    }

    public static function save_postmeta( $post_id ) {

        // Contact Information
        update_post_meta( $post_id, 'email', $_POST['email'] );
        update_post_meta( $post_id, 'phone', $_POST['phone'] );

        // Biography
        update_post_meta( $post_id, 'biography', $_POST['biography'] );

        // Address
        update_post_meta( $post_id, 'add_street', $_POST['add_street'] );
        update_post_meta( $post_id, 'add_street_2', $_POST['add_street_2'] );
        update_post_meta( $post_id, 'add_city', $_POST['add_city'] );
        update_post_meta( $post_id, 'add_state', $_POST['add_state'] );
        update_post_meta( $post_id, 'add_zip', $_POST['add_zip'] );

        // History
        update_post_meta( $post_id, 'hours_spoken', $_POST['hours_spoken'] );
        update_post_meta( $post_id, 'presentations', $_POST['presentations'] );
        update_post_meta( $post_id, 'largest_crowd', $_POST['largest_crowd'] );

        // Programs
        update_post_meta( $post_id, 'program_1', $_POST['program_1'] );
        update_post_meta( $post_id, 'program_1_description', $_POST['program_1_description'] );
        update_post_meta( $post_id, 'program_2', $_POST['program_2'] );
        update_post_meta( $post_id, 'program_2_description', $_POST['program_2_description'] );

        // Testimonials
        update_post_meta( $post_id, 'testimonial_1', $_POST['testimonial_1'] );
        update_post_meta( $post_id, 'testimonial_1_author', $_POST['testimonial_1_author'] );
        update_post_meta( $post_id, 'testimonial_2', $_POST['testimonial_2'] );
        update_post_meta( $post_id, 'testimonial_2_author', $_POST['testimonial_2_author'] );
        update_post_meta( $post_id, 'testimonial_3', $_POST['testimonial_3'] );
        update_post_meta( $post_id, 'testimonial_3_author', $_POST['testimonial_3_author'] );

    }

    /**
     * Labels Define
     *
     * @return array
     */
    private static function labels() {
        return array(
            'name'               => 'Speakers',
            'singular_name'      => 'Speaker',
            'menu_name'          => 'Speakers',
            'name_admin_bar'     => 'Speaker',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Speaker',
            'new_item'           => 'New Speaker',
            'edit_item'          => 'Edit Speaker',
            'view_item'          => 'View Speaker',
            'all_items'          => 'All Speakers',
            'search_items'       => 'Search Speakers',
            'parent_item_colon'  => 'Parent Speakers:',
            'not_found'          => 'No speakers found.',
            'not_found_in_trash' => 'No speakers found in Trash.'
        );
    }

}