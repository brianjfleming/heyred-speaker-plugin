<?php
/**
 * Registration and Callbacks for our Shortcodes
 *
 * @package brianjfleming/heyredspeakers
 */

namespace HeyRed\Speakers;

/**
 * Class HR_Speakers_Settings
 */
class HR_Speakers_Shortcodes {

    // All Registration
    public static function setup()
    {
        $class = '\\' . __NAMESPACE__ . '\\HR_Speakers_Shortcodes';

        add_shortcode('hr_speaker_name', array($class, 'hr_speaker_name'));
        add_shortcode('hr_speaker_email', array($class, 'hr_speaker_email'));
        add_shortcode('hr_speaker_phone', array($class, 'hr_speaker_phone'));
        add_shortcode('hr_speaker_biography', array($class, 'hr_speaker_biography'));

        add_shortcode('hr_speaker_add_street', array($class, 'hr_speaker_add_street'));
        add_shortcode('hr_speaker_add_street_2', array($class, 'hr_speaker_add_street_2'));
        add_shortcode('hr_speaker_add_city', array($class, 'hr_speaker_add_city'));
        add_shortcode('hr_speaker_add_state', array($class, 'hr_speaker_add_state'));
        add_shortcode('hr_speaker_add_zip', array($class, 'hr_speaker_add_zip'));

        add_shortcode('hr_speaker_hours_spoken', array($class, 'hr_speaker_hours_spoken'));
        add_shortcode('hr_speaker_presentations', array($class, 'hr_speaker_presentations'));
        add_shortcode('hr_speaker_largest_crowd', array($class, 'hr_speaker_largest_crowd'));

        add_shortcode('hr_speaker_program_1', array($class, 'hr_speaker_program_1'));
        add_shortcode('hr_speaker_program_1_description', array($class, 'hr_speaker_program_1_description'));
        add_shortcode('hr_speaker_program_2', array($class, 'hr_speaker_program_2'));
        add_shortcode('hr_speaker_program_2_description', array($class, 'hr_speaker_program_2_description'));

        add_shortcode('hr_speaker_testimonial_1', array($class, 'hr_speaker_testimonial_1'));
        add_shortcode('hr_speaker_testimonial_1_author', array($class, 'hr_speaker_testimonial_1_author'));
        add_shortcode('hr_speaker_testimonial_2', array($class, 'hr_speaker_testimonial_2'));
        add_shortcode('hr_speaker_testimonial_2_author', array($class, 'hr_speaker_testimonial_2_author'));
        add_shortcode('hr_speaker_testimonial_3', array($class, 'hr_speaker_testimonial_3'));
        add_shortcode('hr_speaker_testimonial_3_author', array($class, 'hr_speaker_testimonial_3_author'));

        add_shortcode('hr_speaker_registration_form', array( $class, 'hr_speaker_registration_form'));

    }

    public static function hr_speaker_name() {
        return get_the_title();
    }

    public static function hr_speaker_email() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'email', true );
    }

    public static function hr_speaker_phone() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'phone', true );
    }

    public static function hr_speaker_biography() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'biography', true );
    }

    public static function hr_speaker_add_street() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'add_street', true );
    }

    public static function hr_speaker_add_street_2() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'add_street_2', true );
    }

    public static function hr_speaker_add_city() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'add_city', true );
    }

    public static function hr_speaker_add_state() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'add_state', true );
    }

    public static function hr_speaker_add_zip() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'add_zip', true );
    }

    public static function hr_speaker_hours_spoken() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'hours_spoken', true );
    }

    public static function hr_speaker_presentations() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'presentations', true );
    }

    public static function hr_speaker_largest_crowd() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'largest_crowd', true );
    }

    public static function hr_speaker_program_1() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'program_1', true );
    }

    public static function hr_speaker_program_1_description() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'program_1_description', true );
    }

    public static function hr_speaker_program_2() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'program_2', true );
    }

    public static function hr_speaker_program_2_description() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'program_2_description', true );
    }

    public static function hr_speaker_testimonial_1() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'testimonial_1', true );
    }

    public static function hr_speaker_testimonial_1_author() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'testimonial_1_author', true );
    }

    public static function hr_speaker_testimonial_2() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'testimonial_2', true );
    }

    public static function hr_speaker_testimonial_2_author() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'testimonial_2_author', true );
    }

    public static function hr_speaker_testimonial_3() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'testimonial_3', true );
    }

    public static function hr_speaker_testimonial_3_author() {
        global $wp_query;
        $post_id = $wp_query->get_queried_object_id();
        return get_post_meta( $post_id, 'testimonial_3_author', true );
    }

    public static function hr_speaker_registration_form() {


        $post_fake = new \stdClass();
        $post_fake->ID = 0;

        ob_start();

        echo "<form id='hr-speaker-registration-form' name='hr-speaker-registration-form' class='hr-speaker-registration-form'>";

        echo "<div class='form-section'>";
        echo "<h3>Contact Information</h3>";

        // Full Name
        echo '<div class="form-element form-input">';
        echo '<input id="full_name" name="full_name" class="form-element-field" placeholder=" " />';
        echo '<div class="form-element-bar"></div>';
        echo '<label class="form-element-label" for="full_name">Full Name</label>';
        echo '</div>';

        HR_Speakers::render_hr_speaker_contact( $post_fake );
        echo "</div>";

        echo "<div class='form-section'>";
        echo "<h3>Biography</h3>";
        HR_Speakers::render_hr_speaker_biography( $post_fake );
        echo "</div>";

        echo "<div class='form-section'>";
        echo "<h3>Address</h3>";
        HR_Speakers::render_hr_speaker_address( $post_fake );
        echo "</div>";

        echo "<div class='form-section'>";
        echo "<h3>History</h3>";
        HR_Speakers::render_hr_speaker_history( $post_fake );
        echo "</div>";

        echo "<div class='form-section'>";
        echo "<h3>Programs</h3>";
        echo "<p>Please detail two programs you have taken part in, including the program name.</p>";
        HR_Speakers::render_hr_speaker_program_1( $post_fake );
        HR_Speakers::render_hr_speaker_program_2( $post_fake );
        echo "</div>";

        echo "<div class='form-section'>";
        echo "<h3>Testimonials</h3>";
        echo "<p>Please enter up to three testimonials you have received, including author credit.</p>";
        HR_Speakers::render_hr_speaker_testimonial_1( $post_fake );
        HR_Speakers::render_hr_speaker_testimonial_2( $post_fake );
        HR_Speakers::render_hr_speaker_testimonial_3( $post_fake );
        echo "</div>";

        echo '<div class="form-actions">';
        echo '<input type="submit" id="hr-speaker-registration-submit" class="form-btn -nooutline" value="register">';
        echo '</div>';

        echo "</form>";

        $output = ob_get_contents();

        ob_end_clean();


        return $output;



    }

}
